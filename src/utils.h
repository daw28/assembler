/*
 *  utils.h
 *
 *  A file containing useful functions for the DASH shell program
 *
 *  Author: David watson ( with some help from StackOverflow )
 */
#include <assert.h>

/*
 *  Function to read a file into memory and return a char* to the first
 *   byte.
 *  NOTE: returned pointer must be freed by calling code.
 */
char *read_file(FILE *file) {
  long fsize;
  char *str;

  fseek(file, 0, SEEK_END);
  fsize = ftell(file);
  fseek(file, 0, SEEK_SET); // same as rewind(f);
  str = malloc(fsize + 1);
  fread(str, fsize, 1, file);
  fclose(file);

  str[fsize] = 0;
  return str;
}

/*
 *  Function to split a string by a delimiter such as a space or ':'.
 *  Returns an array of char* points where each pointer points to the beginning
 *    of a new string
 *
 *  NOTE: returned pointer must be freed by calling code.
 */
char **str_split(char* in_str, const char a_delim) {
  char *a_str = strdup(in_str);
  char **result = 0;
  size_t count = 0;
  char *tmp = a_str;
  char *last_comma = 0;

  char delim[2];
  delim[0] = a_delim;
  delim[1] = 0;

  /* Count how many elements will be extracted. */
  while (*tmp)
  {
    if (*delim == *tmp)
    {
      count++;
      last_comma = tmp;
    }
    tmp++;
  }

  /* Add space for trailing token. */
  count += last_comma < (a_str + strlen(a_str) - 1);

  /* Add space for terminating null string so caller
     knows where the list of returned strings ends. */
  count++;

  free(a_str);
  a_str = strdup(in_str);
  result = (char **)malloc(sizeof(char *) * count);

  if (result) {
    size_t idx = 0;
    char *token = strtok(a_str, delim);

    while (token) {
      assert(idx < count);
      *(result + idx) = strdup(token);
      idx += 1;
      token = strtok(0, delim);
    }
    *(result + idx) = 0;
  }

  free(a_str);
  return result;
}

/*
 *  Function to split a string by a delimiter such as a space or ':'.
 *  Returns an array of 2 char* points where each pointer points to the beginning
 *    of a new string
 *
 *  NOTE: returned pointers all must be freed by calling code.
 */
char **str_split_once(char* in_str, const char a_delim)
{
    char* tmp = in_str;
    char** out = (char**)malloc( sizeof(char*) * 2 );
    while( *tmp )
    {
        if( *tmp == a_delim )
        {
            out[1] = strdup( tmp+1 );
            out[0] = (char*)malloc( sizeof(char) * ( 1 + tmp - in_str ) );
            memcpy( *out, in_str, ( tmp - in_str ) );
            out[0][tmp - in_str] = 0;
            return out;
        }
        tmp++;
    }
    return NULL;
}

void str_inplace_replace(char* orig, char replace, char with)
{
  for(int i = 0; i < strlen(orig); i++)
  {
    if(orig[i] == replace)
    {
      orig[i] = with;
    }
  }
}
/*
 *  'Find and Replace' type function. Returns a pointer to a newly allocated
 *    string with the contents of the old one after the replacement operation
 *  NOTE: returned pointer must be freed by calling code.
 */
char *str_replace(char *orig, char *replacement, char *with) {
  char *result; // the return pointer
  char *insert; // next insert pointer
  char *tmp;
  int len_rep;  // will hold length of replacements
  int len_with; // will hold length of with
  int len_front;
  int count = 0;

  // sanity checks and initialization
  if (!orig || !replacement)
    return NULL;
  len_rep = strlen(replacement);
  if (len_rep == 0)
    return NULL; // gotta have something to search for
  if (!with)     // with may be NULL, replacement will just get removed
    with = "";
  len_with = strlen(with);

  // count number of replacements needed
  insert = orig;
  while ((tmp = strstr(insert, replacement))) {
    insert = tmp + len_rep;
    count++;
  }

  result = (char *)malloc((strlen(orig) + (len_with - len_rep)) * (count + 1));
  if (!result)
    return NULL;

  tmp = result;
  while (count > 0) {
    // set insert to next occurance of rep in orig
    insert = strstr(orig, replacement);
    len_front = insert - orig; // count number of 'good' bytes
    // copy the good bytes then copy 'with' where rep would be.
    tmp = strncpy(tmp, orig, len_front) + len_front;
    tmp = strncpy(tmp, with, len_with) + len_with;
    // move to point to the end of replacement
    orig += len_front + len_rep;
    count--;
  }
  strcpy(tmp, orig); // copy the remaining bytes
  return result;
}

void print_assembly(uint32_t* source_words)
{
  int line = 0;
  while(*source_words != 0)
  {
      line++;
      printf("%03d: 0x%010x\n",line, *source_words);
      source_words++;
  }
}
