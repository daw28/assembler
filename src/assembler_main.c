#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "binary.h"
#include "utils.h"
#include "assemble.c"



int main(int argc, char** argv)
{
    int c;
    char* outfile = NULL;
    char* infile = NULL;

    opterr = 0;
    while ((c = getopt (argc, argv, "o:")) != -1)
    {
        switch(c)
        {
            case 'o':
                outfile = optarg;
                break;
            default: /* '?' */
                goto input_error;
        }
    }

    infile = argv[optind];
    if(!infile)
    {
        goto input_error;
    }

    if(!outfile)
    {
        outfile = str_replace(infile, ".s", ".bin");
    }

#ifdef DEBUG
    printf("\ninfile: %s\noutfile: %s\n\n", infile, outfile);
#endif

    FILE* in, * out;
    in = fopen(infile, "r");
    out = fopen(outfile, "wb");

    if(!in)
    {
        fprintf( stderr, "Error - Could not open input file: %s\n", infile );
        exit(-1);
    }
    if(!out)
    {
        fprintf( stderr, "Error - Unable to lock output file: %s\n", outfile );
        exit(-1);
    }

    assemble(in, out);

    fclose(in);
    fclose(out);

    return 0;

input_error:
    fprintf(stderr, "Usage: %s [-o outfile] infile\n", argv[0]);
    exit(EXIT_FAILURE);
}
