#define regD_MASK 0x0F000000
#define regS_MASK 0x00F00000
#define regT_MASK 0x0000000F

#define math_imm_MASK 0x0000FFFF
#define math_imm_neg_MASK 0x00008000

#define imm_MASK 0x000fffff
#define imm_neg_MASK 0x00080000

#define instruction_func_MASK 0xF00F0000
#define instruction_MASK 0xF0000000

struct label {
    char* name;
    size_t address;
    size_t index;
    struct label* next;
};

void free_labels( struct label* root )
{
#ifdef DEBUG
    printf( "\nLabels Generated:\n");
#endif

    while(root != NULL)
    {
        struct label* tmp = root->next;
#ifdef DEBUG
        printf( "%s %zu\n", root->name, root->address);
#endif

        free( root->name );
        free( root );
        root = tmp;
    }
}

struct label* find_labels( uint32_t* source, int* out_count )
{
    struct label* root = NULL;
    size_t addr = 0;
    int count = 0;
    while(*source != 0)
    {
        instructionMapping_t* ins = branching_instructions;
        while(ins->name != NULL)
        {
            if((*source & ins->machineword) == ins->machineword)
            {
                char* label = (char*)malloc(sizeof(char) * (5)); // L##:\n
                size_t l_addr = (imm_MASK & *source);
                if(imm_neg_MASK & *source)
                    l_addr = addr - ( ( (~l_addr) & imm_MASK ) );
                else
                    l_addr = addr + l_addr;

                if(l_addr < 300)
                {
                    label[0] = 'L';
                    label[1] = (count/10) + '0';
                    label[2] = (count%10) + '0';
                    label[3] = ':';
                    label[4] = 0;

                    struct label* tmp = root;
                    root = (struct label*)malloc( sizeof(struct label) );
                    root->name = label;
                    root->address = l_addr;
                    root->next = tmp;
                    count++;
                    break;
                }
            }
            ins++;
        }
        addr++;
        source++;
    }
    *out_count = count;
    return root;
}

int print_label( struct label* labels, size_t address )
{
    while(labels != NULL)
    {
        if( labels->address == address )
        {
            printf("                : %s\n", labels->name );
            return 0;
        }
        labels = labels->next;
    }
    return 1;
}

char* get_label( struct label* labels, size_t address )
{
    while(labels != NULL)
    {
        if( labels->address == address )
        {
            return labels->name;
        }
        labels = labels->next;
    }
    return NULL;
}

void print_disassembly(uint32_t* source_words, char** source_lines, struct label* labels)
{
    int line = 0;
    size_t address;
    int do_print_label = 1;
    while( *source_lines != NULL) //*source_words != 0 &&
    {
        if( do_print_label ) // or rather can't print
        {
            if(!print_label( labels, line ))
            {
                do_print_label = 0;
                continue;
            }
        }

        line++;
        do_print_label = 1;
        printf("%03d| 0x%08x : %s\n",line, *source_words, *source_lines);

        source_words++;
        source_lines++;
    }
}

void write_to_file( char** source_lines, struct label* labels, FILE* file )
{
    int line = 0;
    size_t address;
    int do_print_label = 1;
    char* label;
    while( *source_lines != NULL) //*source_words != 0 &&
    {
        if( do_print_label ) // or rather can't print
        {
            if( ( label = get_label( labels, line ) ) )
            {
                do_print_label = 0;
                fwrite(label, strlen(label), 1, file);
                fwrite("\n" , 1, 1, file);
                continue;
            }
        }

        line++;
        do_print_label = 1;
        fwrite(*source_lines, strlen(*source_lines), 1, file);
        fwrite("\n" , 1, 1, file);

        source_lines++;
    }
}

char* process_line( uint32_t machineword, struct label* labels, int line, int total_lines )
{
    instructionMapping_t* instructions;
    instructions = r_type_OPcode;
    while( instructions->name != NULL ) // if R_type_instruction
    {
        if((machineword & instruction_func_MASK) == instructions->machineword)
        {
            char* out = (char*)malloc(80);

            uint8_t regD = (uint8_t)((machineword & regD_MASK) >> 24);
            uint8_t regS = (uint8_t)((machineword & regS_MASK) >> 20);
            uint8_t regT = (uint8_t)(machineword & regT_MASK);

            sprintf(out, "%s $%d,$%d,$%d", instructions->name, regD, regS, regT);

            return out;
        }
        instructions++;
    }

    instructions = i_type_OPcode;
    while( instructions->name != NULL ) // if I_type_instruction
    {
        if((machineword & instruction_func_MASK) == instructions->machineword)
        {
            // it's an I type! TODO
            //return "I-Type!:D";
            char* out = (char*)malloc(80);

            uint8_t regD = (uint8_t)((machineword & regD_MASK) >> 24);
            uint8_t regS = (uint8_t)((machineword & regS_MASK) >> 20);
            int imm;

            if(machineword & math_imm_neg_MASK)
            {
                imm = ((~machineword) & math_imm_MASK) * -1;
            }
            else
            {
                imm = (machineword & math_imm_MASK);
            }

            sprintf(out, "%s $%d,$%d,%d", instructions->name, regD, regS, imm);

            return out;
        }
        instructions++;
    }

    instructions = j_type_OPcode;
    while( instructions->name != NULL ) // if J_type_instruction
    {
        if((machineword & instruction_MASK) == instructions->machineword)
        {
            char* out = (char*)malloc(80);
            char* label;

            uint8_t regD = (uint8_t)((machineword & regD_MASK) >> 24);
            uint8_t regS = (uint8_t)((machineword & regS_MASK) >> 20);
            int imm;

            if(machineword & imm_neg_MASK)
            {
                imm = ((~machineword) & imm_MASK) * -1;
            }
            else
            {
                imm = (machineword & imm_MASK);
            }

            switch(instructions->machineword)
            {
                case 0x40000000: // j
                    label = get_label( labels, line + imm);
                    if(label && (line + imm) < total_lines  && (line+imm) > -1)
                        sprintf(out, "j %s", label);
                    else
                        sprintf(out, "j %d", imm);
                    break;
                case 0xa0000000: // bnez
                case 0xb0000000: // beqz
                    label = get_label( labels, line + imm);
                    if(label && (line + imm) < total_lines  && (line+imm) > -1)
                        sprintf(out, "%s $%d,%s", instructions->name, regD, label);
                    else
                        sprintf(out, "%s $%d,%d", instructions->name, regD, imm);
                    break;
                case 0x80000000: // sw
                case 0x90000000: // lw
                    sprintf(out, "%s $%d,%d($%d)", instructions->name, regD, imm, regS);
                    break;
            }

            return out;
        }
        instructions++;
    }

    char* out = (char*)malloc(17);
    sprintf(out, ".word 0x%08x", machineword);
    return out;
}

void disassemble(FILE* infile, FILE* outfile, int print_to_console, int save_outfile)
{
    uint32_t* source_words = (uint32_t*)read_file( infile );
    // find instructions that relate to labels
    int num_labels = 0;
    struct label* labels = find_labels( source_words, &num_labels );
    // count number of output lines and malloc a buffer for the lines
    int num_lines = 0;
    uint32_t* lines = source_words;
    while ( *lines != 0 ) {
        lines++;
        num_lines++;
    }
#ifdef DEBUG
    printf("lines: %d\n", num_lines);
#endif
    char** output_buffer_lines = (char**)malloc( sizeof(char*) * (num_lines + 1) );
    output_buffer_lines[num_lines] = NULL;

    for( int i = 0; i < num_lines; i++)
    {
        output_buffer_lines[i] = process_line( source_words[i], labels, i, num_lines );
    }

    if(print_to_console)
    {
        print_disassembly(source_words, output_buffer_lines, labels);
    }

    if(save_outfile)
    {
        write_to_file( output_buffer_lines, labels, outfile);
    }

    for( int i = 0; i < num_lines; i++)
    {
        free(output_buffer_lines[i]);
    }

    free(output_buffer_lines);
    free(source_words);
    free_labels( labels );
}
