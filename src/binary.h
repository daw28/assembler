/*
 * This file contains mappings between the human readable assembly code
 * experssions
 *  and their machine code binary counterparts
 * Author: David Watson
 */

#include <stdint.h>

typedef struct {
  const char *name;
  uint8_t machinebyte;
} registerMapping_t;

typedef struct {
  const char *name;
  uint32_t machineword;
} instructionMapping_t;

// Register Mappings
registerMapping_t registerMap[] = {{"$0", 0x00},
                                   {"$1", 0x01},
                                   {"$2", 0x02},
                                   {"$3", 0x03},
                                   {"$4", 0x04},
                                   {"$5", 0x05},
                                   {"$6", 0x06},
                                   {"$7", 0x07},
                                   {"$8", 0x08},
                                   {"$9", 0x09},
                                   {"$10", 0x0a},
                                   {"$11", 0x0b},
                                   {"$12", 0x0c},
                                   {"$13", 0x0d},
                                   {"$14", 0x0e},
                                   {"$sp", 0x0e},
                                   {"$15", 0x0f},
                                   {"$ra", 0x0f},
                                   {NULL, 0}};

// R-Type Opcodes
instructionMapping_t r_type_OPcode[] = {{"add", 0x00000000},
                                        {"sub", 0x00020000},
                                        {"and", 0x000b0000},
                                        {"or", 0x000d0000},
                                        {"xor", 0x000f0000},
                                        {NULL, 0}};

// I-Type Opcodes
instructionMapping_t i_type_OPcode[] = {{"addi", 0x10000000},
                                        {"subi", 0x10020000},
                                        {"andi", 0x100b0000},
                                        {"ori", 0x100d0000},
                                        {"xori", 0x100f0000},
                                        {NULL, 0}};

// J-Type Opcodes
instructionMapping_t j_type_OPcode[] = {{"lw", 0x80000000},
                                        {"sw", 0x90000000},
                                        {"j", 0x40000000},
                                        {"beqz", 0xa0000000},
                                        {"bnez", 0xb0000000},
                                        {NULL, 0}};
// disassembly label targets
instructionMapping_t branching_instructions[] = {{"j", 0x40000000},
                                                 {"beqz", 0xa0000000},
                                                 {"bnez", 0xb0000000},
                                                 {NULL, 0}};
