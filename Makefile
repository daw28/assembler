all: assembler disassembler


assembler: src/assembler_main.c src/assemble.c src/utils.h src/binary.h
	gcc -o assembler src/assembler_main.c

assembler_debug: src/assembler_main.c src/assemble.c src/utils.h src/binary.h
	gcc -g -DDEBUG -o assembler src/assembler_main.c

disassembler: src/disassembler_main.c src/disassemble.c src/utils.h src/binary.h
	gcc -o disassembler src/disassembler_main.c

disassembler_debug: src/disassembler_main.c src/disassemble.c src/utils.h src/binary.h
	gcc -g -DDEBUG -o disassembler src/disassembler_main.c

clean:
	rm assembler disassembler *.bin


test: assembler_debug
	for i in {1..3}; do ./assembler -o test$$i.bin tests/test$$i.s; done
	diff test1.bin tests/test1.bin
	diff test2.bin tests/test2.bin
	diff test3.bin tests/test3.bin

.PHONY: clean test
