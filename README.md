# Simplified WRAMP Assembler #

This assembler was written to satisfy Assignment 1 of COMP311-17B at Waikato University.

An assembler and disassembler program for a subset of the WRAMP instruction set.

## Compilation ##

A Makefile is included to build the source files.

type `make` to compile both executables or `make [assembler|disassembler]` to compile one at a time.

## Assembler ##

`./assembler [-o outfile.bin] infile.s`
Specifying an output file is optional with `-o` and defaults to a .bin with the same name as the in file.
The program will log any errors to `stderr` and outputs the binary to the out file. 

## Disassembler ##

`./disassembler [-o outfile.s] infile.bin`
Specifying an output file is optional with `-o` if no output file is given the disassembly is printed to console. To print disassembly and save to an output file, add the `-p` option.

### Example Output ###

```
                : L00:
006| 0x89a00000 : lw $9,0($10)
007| 0x18820001 : subi $8,$8,1
008| 0x1ff0beef : addi $15,$15,-16656
009| 0x134b0fff : andi $3,$4,4095
010| 0x156ff0f0 : xori $5,$6,-3855
011| 0x9ef00000 : sw $14,0($15)
012| 0xb08ffff9 : bnez $0,L00:
013| 0x4000018f : j 399

```

### Implemented Instructions ###


| Instruction          | Format                                  | Effect                       |
|----------------------|-----------------------------------------|------------------------------|
| add Rd, Rs, Rt   | 0000 dddd ssss 0000 0000 0000 0000 tttt | Rd = Rs + Rt                 |
| sub Rd, Rs, Rt   | 0000 dddd ssss 0010 0000 0000 0000 tttt | Rd = Rs − Rt                 |
| and Rd, Rs, Rt   | 0000 dddd ssss 1011 0000 0000 0000 tttt | Rd = Rs ∧ Rt                 |
| xor Rd, Rs, Rt   | 0000 dddd ssss 1111 0000 0000 0000 tttt | Rd = Rs ⊕ Rt                 |
| or Rd, Rs, Rt    | 0000 dddd ssss 1101 0000 0000 0000 tttt | Rd = Rs ∨ Rt                 |
| addi Rd, Rs, imm | 0001 dddd ssss 0000 iiii iiii iiii iiii | Rd = Rs + imm                |
| subi Rd, Rs, imm | 0001 dddd ssss 0010 iiii iiii iiii iiii | Rd = Rs − imm                |
| andi Rd, Rs, imm | 0001 dddd ssss 1011 iiii iiii iiii iiii | Rd = Rs ∧ imm                |
| xori Rd, Rs, imm | 0001 dddd ssss 1111 iiii iiii iiii iiii | Rd = Rs ⊕ imm                |
| ori Rd, Rs, imm  | 0001 dddd ssss 1101 iiii iiii iiii iiii | Rd = Rs ∨ imm                |
| lw Rd, imm(Rs)     | 1000 dddd ssss iiii iiii iiii iiii iiii | Rd = Mem[Rs + imm]           |
| sw Rd, imm(Rs)     | 1001 dddd ssss iiii iiii iiii iiii iiii | Mem[Rs + imm] = Rd           |
| j imm                | 0100 0000 0000 iiii iiii iiii iiii iiii | P C = imm                    |
| bnez Rs, imm       | 1011 0000 ssss iiii iiii iiii iiii iiii | if Rs != 0 : P C = P C + imm |
| beqz Rs, imm       | 1010 0000 ssss iiii iiii iiii iiii iiii | if Rs = 0 : P C = P C + imm  |
